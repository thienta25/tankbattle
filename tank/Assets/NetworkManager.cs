﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour
{
	private const string typeName = "UniqueGameName";
	private const string gameName = "Tank Room";
	
	private bool isRefreshingHostList = false;
	static public bool isSpawnEnemy=false;
	private HostData[] hostList;
	private int count = 0;

	static public int choice = 1;

	public GameObject playerPrefab;
	public GameObject playerPrefab2;

	void Start () {
		choice = ChangeScreen.choice;
		if (choice == 1) {
			Instantiate(playerPrefab, playerPrefab.transform.position, Quaternion.identity);
			isSpawnEnemy = true;
		}
	}

	void OnGUI()
	{
		if (choice == 1) {
			//Instantiate(playerPrefab, playerPrefab.transform.position, Quaternion.identity);
		}
		else if (choice == 2) {
			if (!Network.isClient && !Network.isServer) {
				if (GUI.Button (new Rect (30, 100, 100, 70), "Start Server"))
					StartServer ();
			
				if (GUI.Button (new Rect (30, 200, 100, 70), "Refresh Hosts"))
					RefreshHostList ();

				if (hostList != null) {
					for (int i = 0; i < hostList.Length; i++) {
						if (GUI.Button (new Rect (30, 300 + (110 * i), 100, 70), hostList [i].gameName))
							JoinServer (hostList [i]);
					}
				}
			}
		}
	}

	private void StartServer()
	{
		if(hostList!=null)
		for (int i = 0; i < hostList.Length; i++) {
			Network.Disconnect ();
			MasterServer.ClearHostList ();
			MasterServer.UnregisterHost ();
		}

		Network.InitializeServer(5, 25000, !Network.HavePublicAddress());
		MasterServer.RegisterHost(typeName, gameName);
	}
	
	void OnServerInitialized()
	{
		//SpawnPlayer();
		Vector3 a=new Vector3(-16,0,-44);
		Network.Instantiate(playerPrefab, a, Quaternion.identity, 0);
	}
	
	
	void Update()
	{
		if (isRefreshingHostList && MasterServer.PollHostList().Length > 0)
		{
			isRefreshingHostList = false;
			hostList = MasterServer.PollHostList();
		}
	}
	
	private void RefreshHostList()
	{
		if (!isRefreshingHostList)
		{
			isRefreshingHostList = true;
			MasterServer.RequestHostList(typeName);
		}
	}
	
	
	private void JoinServer(HostData hostData)
	{
		Network.Connect(hostData);
	}
	
	void OnConnectedToServer()
	{
		Debug.Log ("Connect to server");
		spawnEnemy();
		Network.Instantiate(playerPrefab2, playerPrefab2.transform.position, Quaternion.identity, 0);
	}

	void OnPlayerConnected(){
		/*
		count++;
		Debug.Log ("count = " + count);
		if (count == 2) {
			spawnEnemy();
		}
		*/
	}

	[RPC] 
	public void spawnEnemy()
	{
		isSpawnEnemy = true;
		if (GetComponent<NetworkView> ().isMine) {
			GetComponent<NetworkView> ().RPC ("spawnEnemy", RPCMode.OthersBuffered);
		}
	}	
}
