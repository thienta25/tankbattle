using UnityEngine;
using System.Collections;

public class spawner : MonoBehaviour {
	
	public int count = 2;
	public float time = 8;
	float timer;
	public bool onStart = true;
	public GameObject prefab;
	public GameObject FX;
	// Use this for initialization
	void Start () {
		timer = time;
		//if (onStart)
		//	Spawn();
	}
	
	// Update is called once per frame
	void Update () {
		if(NetworkManager.isSpawnEnemy)
		{
			timer -= Time.deltaTime;
		
			if (timer <= 0) {
				timer = time;
				if (count <= 0) {
					this.enabled = false;
					return;
				}
				Spawn ();
			}
		}
	}
	
	void Spawn()
	{
		count--;
		if (NetworkManager.choice == 2) {
			Network.Instantiate (prefab, transform.position, Quaternion.identity, 0);
			Network.Instantiate (FX, transform.position, Quaternion.identity, 0);
		} else {
			Instantiate (prefab, transform.position, Quaternion.identity);
			Instantiate (FX, transform.position, Quaternion.identity);
		}
	}
}
