using UnityEngine;
using System.Collections;


public class Item : MonoBehaviour {
	public float timeAppear = 2;
	public float timeDisappear=5;
	public bool isAppear=true;
	public float timer;
	public GameObject prefab;
	public Object itemObject;

	void Start () {
		timer = timeDisappear;
		prefab = GameObject.Instantiate( UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Art/prefabs/Sphere.prefab", typeof(GameObject)) ) as GameObject;
		//prefab = GameObject.Instantiate(Resources.Load("Assets/Art/prefabs/Sphere.prefab") ) as GameObject;
		//prefab = (GameObject)Resources.Load("Assets/Art/prefabs/Sphere.prefab");
		prefab.transform.position = transform.position;
		//prefab = (GameObject)Resources.Load("Assets/Art/prefabs/Sphere.prefab");
		//SpawnItem ();
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
			
		if (timer <= 0&&isAppear)
				DisSpawnItem ();
	}

	void SpawnItem()
	{
		if (NetworkManager.choice == 2) {
			itemObject = Network.Instantiate (prefab, transform.position, Quaternion.identity, 0);
		} else if (NetworkManager.choice == 1) {
			itemObject = Instantiate (prefab, transform.position, Quaternion.identity);
		}
	}
	public void DisSpawnItem(){
	// Update is called once per frame
		if(itemObject!=null) DestroyObject (itemObject);
		DestroyObject (this.gameObject);
	}
}

