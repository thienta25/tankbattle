﻿using UnityEngine;

using System.Collections;

public class ChangeScreen : MonoBehaviour {

	static public int choice;

	public void ChangeToScene (int _choice) {
		choice = _choice;
		Application.LoadLevel ("tanks");
	}

	public void Quit() {
		Application.Quit ();
	}
}
