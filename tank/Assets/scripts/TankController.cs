using UnityEngine;
using System.Collections;

public class TankController : MonoBehaviour {
	
	public enum goDirection{ up, down, left, right, stay};
	public goDirection direction;
	
	public float speed;
	public GameObject mBullet;
	bool shouldGo = false;
	
	bool haveItemFast =false;

	Quaternion initialRot;
	// Use this for initialization

	private float lastSynchronizationTime = 0f;
	private float syncDelay = 0f;
	private float syncTime = 0f;
	private float timerItem = 0f;
	private Vector3 syncStartPosition = Vector3.zero;
	private Vector3 syncEndPosition = Vector3.zero;

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
	{
		Vector3 syncPosition = Vector3.zero;
		Vector3 syncVelocity = Vector3.zero;
		if (stream.isWriting)
		{
			syncPosition = GetComponent<Rigidbody>().position;
			stream.Serialize(ref syncPosition);
			
			syncVelocity = GetComponent<Rigidbody>().velocity;
			stream.Serialize(ref syncVelocity);
		}
		else
		{
			stream.Serialize(ref syncPosition);
			stream.Serialize(ref syncVelocity);
			
			syncTime = 0f;
			syncDelay = Time.time - lastSynchronizationTime;
			lastSynchronizationTime = Time.time;
			
			syncEndPosition = syncPosition + syncVelocity * syncDelay;
			syncStartPosition = GetComponent<Rigidbody>().position;
		}
	}
	
	void Awake()
	{
		lastSynchronizationTime = Time.time;
	}


	void Start () 
	{
		initialRot = transform.rotation;
	}

	private void SyncedMovement()
	{
		syncTime += Time.deltaTime;
		GetComponent<Rigidbody>().position = Vector3.Lerp(syncStartPosition, syncEndPosition, syncTime / syncDelay);
	}

	void Update()
	{
		if (NetworkManager.choice == 2) {
			if (GetComponent<NetworkView> ().isMine) {
				InputMovement ();
				if (Input.GetKeyDown (KeyCode.Space) /*&& !Bullet.isBulletAlive*/) {
					Shoot ();
				}
			}
			else{
				SyncedMovement();
			}
		} else if (NetworkManager.choice == 1) {
			InputMovement ();
			if (Input.GetKeyDown (KeyCode.Space) /*&& !Bullet.isBulletAlive*/) {
				Shoot ();
			}
		}

		//timer for item
		if (haveItemFast) {
			timerItem+=Time.deltaTime;
			if(timerItem>=15){
				timerItem=0f;
				haveItemFast=false;
				speed=15;
			}
		}
		//else
			//SyncedMovement();

	}


	private void InputMovement()
	{
		GetComponent<Rigidbody>().velocity = Vector3.zero; 
		
		if (direction == goDirection.right)
		{
			transform.rotation = initialRot;
			transform.Rotate(new Vector3(0, 180, 0));
			
			GetComponent<Rigidbody>().AddForce(-transform.right * speed, ForceMode.Impulse);
		}else if (direction == goDirection.left)
		{
			transform.rotation = initialRot;
			transform.Rotate(new Vector3(0, 0, 0));
			
			
			GetComponent<Rigidbody>().AddForce(-transform.right * speed, ForceMode.Impulse);
		}else if (direction == goDirection.up)
		{
			transform.rotation = initialRot;
			transform.Rotate(new Vector3(0, 90, 0));
			
			GetComponent<Rigidbody>().AddForce(-transform.right * speed, ForceMode.Impulse);
		}else if (direction == goDirection.down)
		{
			transform.rotation = initialRot;
			transform.Rotate(new Vector3(0, -90, 0));
			
			GetComponent<Rigidbody>().AddForce(-transform.right * speed, ForceMode.Impulse);
		}
		
		// SHOOT
		
		
		if (!GetComponent<Animation>().IsPlaying("shoot") && !GetComponent<Animation>().IsPlaying("idle"))// && animation["shoot"].time >= animation["shoot"].length)
		{
			GetComponent<Animation>().Play("idle");
		}
	}

	void OnCollisionEnter(Collision col)
	{
		//isBulletAlive = false;
		if (col.gameObject.tag == "itemTag") {
			//col.gameObject.GetComponent<Item>().DisSpawnItem();
			DestroyObject(col.gameObject);
			speed=30;
			haveItemFast=true;
		}

	}

	[RPC] 
	public void Shoot()
	{

			GetComponent<Animation> ().Play ("shoot");
			GameObject bullet = Instantiate (mBullet, transform.position + -transform.right * 7, this.transform.rotation) as GameObject;
			(bullet.GetComponent<Bullet> () as Bullet).owner = this.gameObject;
		if (NetworkManager.choice == 2) {
			if (GetComponent<NetworkView> ().isMine) {
				GetComponent<NetworkView> ().RPC ("Shoot", RPCMode.OthersBuffered);
			}
		}
	}
}
